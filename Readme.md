 vetted_cp_sample
========================

REST API implementation in Django for managing products of an online company profiling platform. 

How to run?
------------

* `git clone https://namc@bitbucket.org/namc/vetted_cp_sample.git`

* `cd vetted_cp_sample`

* `sudo pip install virtualenv`

* `virtualenv vet`

* `source vet/bin/activate`

* `pip install -r requirement.txt` 

* `python manage.py migrate`

* Load database with some initial data as fixtures :

     * `python manage.py loaddata users` 
        * will load 2 dummy users with usernames: _userAdmin_, _userPass_ and password: _testpass_
        * this can be used to view admin panel.

     * `python manage.py loaddata companies` (will load a company list)

* `python manage.py runserver` 
    * test your application running on [http://127.0.0.1:8000/](http://127.0.0.1:8000/)


API Requests
------------

We can make GET, POST, PUT requests using django forms and templates. 

URLS
------------

* Homepage - `http://127.0.0.1:8000` 

* Detail - `http://127.0.0.1:8000/<company_id>`

* Edit - `http://127.0.0.1:8000/<company_id>/edit`

* Add New - `http://127.0.0.1:8000/new`





