from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

from django.conf.urls import patterns, include, url

urlpatterns = patterns('search.views',
	url(r'^$', 'index', name="index"),
	url(r'^(?P<company_id>\d+)/$', 'detail', name="detail"),
	url(r'^new/$', 'addCompany', name="addCompany"),
	url(r'^(?P<company_id>\d+)/edit/$', 'editCompany', name="editCompany"),
)
