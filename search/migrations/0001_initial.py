# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=512)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('description', models.TextField(blank=True)),
                ('location', models.CharField(max_length=100)),
                ('category', models.CharField(default=b'', max_length=20, blank=True)),
                ('website', models.URLField(blank=True)),
                ('raised', models.IntegerField(blank=True)),
                ('employees', models.IntegerField(blank=True)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
    ]
