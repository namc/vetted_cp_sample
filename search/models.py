from django.db import models

# Create your models here.

class Company(models.Model):
    name = models.CharField(max_length=512, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=True)
    location = models.CharField(max_length=100, blank=False)
    category = models.CharField(max_length=20, blank=False, default='')
    website = models.URLField(blank=True)
    raised = models.IntegerField(blank=True)
    employees = models.IntegerField(blank=True)

    class Meta:
        ordering = ('created',)