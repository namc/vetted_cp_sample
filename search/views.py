from django.http import HttpResponse
from django.template import loader
from django.http import Http404
from django.shortcuts import render, get_object_or_404

from .models import Company
from .forms import CompanyForm

def index(request):
    company_list = Company.objects.order_by('-created')[:10]
    template = loader.get_template('index.html')
    context = {
        'company_list': company_list,
    }
    return HttpResponse(template.render(context, request))

def detail(request, company_id):
    company = Company.objects.get(id=company_id)
    template = loader.get_template('detail.html')
    context = {
        'company': company,
    }
    return HttpResponse(template.render(context, request))

def addCompany(request):
    if request.method == "POST":
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = form.save(commit=False)
            company.created = timezone.now()
            company.save()
            return redirect('index.html')
    else:
        form = CompanyForm()
    return render(request, 'edit.html', {'form': form})

def editCompany(request, company_id):
    post = get_object_or_404(Company, id=company_id)
    if request.method == "POST":
        form = CompanyForm(request.POST, instance=post)
        if form.is_valid():
            company = form.save(commit=False)
            company.created = timezone.now()
            company.save()
            return redirect('detail.html', id=company.id)
    else:
        form = CompanyForm(instance=post)
    return render(request, 'edit.html', {'form': form})
