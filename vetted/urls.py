from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'vetted.views.home', name='home'),
    url(r'^', include('search.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
